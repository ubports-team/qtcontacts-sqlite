Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: qtcontacts-sqlite
Upstream-Contact: UBports developers <devs@ubports.com>
Source: https://gitlab.com/ubports/development/core/qtcontacts-sqlite

Files: .building
 .gitignore
 README
 config.pri
 qtcontacts-sqlite.pro
 rpm/qtcontacts-sqlite-qt5.spec
 src/engine/.gitignore
 src/engine/engine.pro
 src/engine/plugin.json
 src/extensions/QContactChangesFetchRequest
 src/extensions/QContactChangesSaveRequest
 src/extensions/QContactClearChangeFlagsRequest
 src/extensions/QContactCollectionChangesFetchRequest
 src/extensions/QContactDeactivated
 src/extensions/QContactDetailFetchRequest
 src/extensions/QContactOriginMetadata
 src/extensions/QContactStatusFlags
 src/extensions/QContactUndelete
 src/qtcontacts-sqlite-qt5-extensions.pc
 src/src.pro
 tests/.gitignore
 tests/auto/aggregation/aggregation.pro
 tests/auto/auto.pro
 tests/auto/database/database.pro
 tests/auto/detailfetchrequest/detailfetchrequest.pro
 tests/auto/displaylabelgroups/displaylabelgroups.pro
 tests/auto/displaylabelgroups/test/test.pro
 tests/auto/displaylabelgroups/testplugin/testplugin.pro
 tests/auto/memorytable/memorytable.pro
 tests/auto/phonenumber/phonenumber.pro
 tests/auto/qcontactmanager/qcontactmanager.pro
 tests/auto/qcontactmanagerfiltering/qcontactmanagerfiltering.pro
 tests/auto/synctransactions/synctransactions.pro
 tests/benchmarks/benchmarks.pro
 tests/benchmarks/deltadetection/deltadetection.pro
 tests/benchmarks/fetchtimes/fetchtimes.pro
 tests/common.pri
 tests/run_test.sh
 tests/tests.pro
 tests/tests.xml
Copyright: 2013-2019, Jolla Ltd.
  2019-2020, Open Mobile Platform LLC.
License: BSD-3-clause
Comment:
 Files listed come without copyright and license information.
 Assuming major copyright holders and license from majority of
 other project files.

Files: src/engine/contactid.cpp
 src/engine/contactid_p.h
 src/engine/contactnotifier.cpp
 src/engine/contactnotifier.h
 src/engine/contactreader.cpp
 src/engine/contactreader.h
 src/engine/contactsdatabase.cpp
 src/engine/contactsdatabase.h
 src/engine/contactsengine.cpp
 src/engine/contactsengine.h
 src/engine/contactwriter.cpp
 src/engine/contactwriter.h
 src/extensions/contactdelta.h
 src/extensions/contactdelta_impl.h
 src/extensions/contactmanagerengine.h
 src/extensions/qcontactstatusflags.h
 src/extensions/qcontactundelete.h
 src/extensions/qcontactundelete_impl.h
 src/extensions/qtcontacts-extensions.h
 src/extensions/qtcontacts-extensions_impl.h
 src/extensions/twowaycontactsyncadaptor.h
 src/extensions/twowaycontactsyncadaptor_impl.h
 tests/auto/aggregation/tst_aggregation.cpp
 tests/auto/displaylabelgroups/test/tst_displaylabelgroups.cpp
 tests/auto/synctransactions/testsyncadaptor.cpp
 tests/auto/synctransactions/testsyncadaptor.h
 tests/benchmarks/fetchtimes/main.cpp
Copyright: 2013, Jolla Ltd.
  2013-2014, Jolla Ltd.
  2013-2019, Jolla Ltd.
  2014, Jolla Ltd.
  2014-2015, Jolla Ltd.
  2014-2016, Jolla Ltd.
  2014-2017, Jolla Ltd.
  2019, Jolla Ltd.
  2019-2020, Open Mobile Platform LLC.
  2020, Open Mobile Platform LLC.
License: BSD-3-clause

Files: src/extensions/qcontactchangesfetchrequest.h
 src/extensions/qcontactchangesfetchrequest_impl.h
 src/extensions/qcontactchangesfetchrequest_p.h
 src/extensions/qcontactchangessaverequest.h
 src/extensions/qcontactchangessaverequest_impl.h
 src/extensions/qcontactchangessaverequest_p.h
 src/extensions/qcontactclearchangeflagsrequest.h
 src/extensions/qcontactclearchangeflagsrequest_impl.h
 src/extensions/qcontactclearchangeflagsrequest_p.h
 src/extensions/qcontactcollectionchangesfetchrequest.h
 src/extensions/qcontactcollectionchangesfetchrequest_impl.h
 src/extensions/qcontactcollectionchangesfetchrequest_p.h
 src/extensions/qcontactdetailfetchrequest.h
 src/extensions/qcontactdetailfetchrequest_impl.h
 src/extensions/qcontactdetailfetchrequest_p.h
 tests/auto/detailfetchrequest/tst_detailfetchrequest.cpp
 tests/auto/synctransactions/tst_synctransactions.cpp
Copyright: 2019, Open Mobile Platform LLC.
  2020, Open Mobile Platform LLC.
License: BSD-3-clause

Files: src/engine/contactstransientstore.cpp
 src/engine/contactstransientstore.h
 src/engine/memorytable.cpp
 src/engine/memorytable_p.h
 src/engine/trace_p.h
 tests/auto/memorytable/tst_memorytable.cpp
 tests/auto/synctransactions/testsyncadapter.cpp
 tests/auto/synctransactions/testsyncadapter.h
 tests/benchmarks/deltadetection/deltasyncadapter.cpp
 tests/benchmarks/deltadetection/deltasyncadapter.h
 tests/benchmarks/deltadetection/main.cpp
Copyright: 2013, Jolla Ltd.
  2014, Jolla Ltd.
  2016, Jolla Ltd.
License: BSD-3-clause

Files: src/engine/conversion.cpp
 src/engine/conversion_p.h
 src/engine/semaphore_p.cpp
 src/engine/semaphore_p.h
 src/extensions/qtcontacts-extensions_manager_impl.h
 tests/auto/phonenumber/tst_phonenumber.cpp
Copyright: 2013, Jolla Ltd. <matthew.vogt@jollamobile.com>
License: BSD-3-clause

Files: src/engine/defaultdlggenerator.cpp
 src/engine/defaultdlggenerator.h
 src/extensions/displaylabelgroupgenerator.h
 tests/auto/displaylabelgroups/testplugin/testdlggplugin.cpp
 tests/auto/displaylabelgroups/testplugin/testdlggplugin.h
Copyright: 2019, Jolla Ltd. <chris.adams@jollamobile.com>
License: BSD-3-clause

Files: src/extensions/qcontactdeactivated.h
 src/extensions/qcontactdeactivated_impl.h
 src/extensions/qcontactoriginmetadata.h
 src/extensions/qcontactoriginmetadata_impl.h
 src/extensions/qcontactstatusflags_impl.h
Copyright: 2013, Jolla Ltd. <mattthew.vogt@jollamobile.com>
  2014, Jolla Ltd. <mattthew.vogt@jollamobile.com>
License: BSD-3-clause

Files: tests/auto/qcontactmanagerfiltering/tst_qcontactmanagerfiltering.cpp
 tests/qcontactmanagerdataholder.h
Copyright: 2012, Digia Plc and/or its subsidiary(-ies).
License: (GPL-3 and/or LGPL-2.1) with Qt-LGPL-1.1 exception

Files: tests/auto/qcontactmanager/tst_qcontactmanager.cpp
 tests/util.h
Copyright: 2012, Digia Plc and/or its subsidiary(-ies).
  2020, Open Mobile Platform LLC.
License: (GPL-3 and/or LGPL-2.1) with Qt-LGPL-1.1 exception

Files: tests/auto/database/stub_contactsengine.cpp
 tests/auto/database/tst_database.cpp
Copyright: 2014, Jolla Ltd. <richard.braakman@jollamobile.com>
License: BSD-3-clause

Files: src/engine/contactsplugin.cpp
Copyright: 2013, Jolla Ltd. <andrew.den.exter@jollamobile.com>
License: BSD-3-clause

Files: debian/*
Copyright: 2017-2022, Alberto Mardegan <mardy@users.sourceforge.net>
  2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: BSD-3-clause or GPL-3 or ((GPL-3 and/or LGPL-2.1) with Qt-LGPL-1.1 exception)

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: (GPL-3 and/or LGPL-2.1) with Qt-LGPL-1.1 exception
 Alternatively, this file may be used under the terms of the GNU Lesser
 General Public License version 2.1 as published by the Free Software
 Foundation and appearing in the file LICENSE.LGPL included in the
 packaging of this file.  Please review the following information to
 ensure the GNU Lesser General Public License version 2.1 requirements
 will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 .
 In addition, as a special exception, Digia gives you certain additional
 rights.  These rights are described in the Digia Qt LGPL Exception
 version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
 .
 GNU General Public License Usage
 Alternatively, this file may be used under the terms of the GNU
 General Public License version 3.0 as published by the Free Software
 Foundation and appearing in the file LICENSE.GPL included in the
 packaging of this file.  Please review the following information to
 ensure the GNU General Public License version 3.0 requirements will be
 met: http://www.gnu.org/copyleft/gpl.html.

License: GPL-3
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-3".
