qtcontacts-sqlite (0.3.20-3) unstable; urgency=medium

  * debian/patches:
    + Add 2002_adjust-icu-extension-name.patch. Make ICU extension loading
      process more configurable.
  * debian/control:
    + Add to B-D:/D: libsqlite3-ext-icu.
  * debian/rules:
    + Configure SQLITE_EXTENSIONS and SQLITE_ICU_EXT qmake variables
      appropriately for sqlite3 on Debian.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 08 Mar 2025 21:53:45 +0100

qtcontacts-sqlite (0.3.20-2) unstable; urgency=medium

  * debian/control:
    + Add to R: (libqt5contact5-plugin-sqlite): contactsd.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 10 Feb 2025 16:29:36 +0100

qtcontacts-sqlite (0.3.20-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Trivially rebase patches 1001, 1003, 1004, 1005.
    + Reduce 2001_disable-failing-tests.patch. Two tests not skipped anymore.
    + Rebase 2001_disable-failing-tests.patch.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 05 Oct 2024 18:04:26 +0200

qtcontacts-sqlite (0.3.19-1) unstable; urgency=medium

  [ Guido Berhoerster ]
  * New upstream version, this time from official upstream (SailfishOS),
    instead of the non-clean UBports fork of qtcontacts-sqlite.
  * debian/{watch,control,copyright}:
    + Use correct upstream.
  * debian/patches:
    + Drop 1002_typo-fix.patch (obsolete, fixed upstream)
    + Add patch series from UBports (patches 1001 - 1006).
  * debian/control:
    + Drop dependency on sqlite-icu-extension
  * debian/rules:
    + Allow tests to work without system GSettings schema.

  [ Mike Gabriel ]
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/patches:
    + Add 2001_disable-failing-tests.patch. Disable failing tests.
  * debian/rules:
    + Clean-up schemas/gschemas.compiled during dh_clean.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 12 May 2024 23:25:01 +0200

qtcontacts-sqlite (0.3.5-1) unstable; urgency=medium

  * Initial upload to Debian. (Closes: #1070468).

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 05 May 2024 23:58:12 +0200
